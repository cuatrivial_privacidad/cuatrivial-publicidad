# Privacy Policy

Your Company Name ("us", "we", or "our") operates the Your App Name mobile application (the "Service").

This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.

## Information Collection And Use

We do not collect any personal data while you use our Service.

## Types of Data Collected

### Personal Data

While using our Service, we do not ask you to provide us with any personally identifiable information.

## Use of Data

Your Company Name does not use the collected data for any purpose because no data is collected.

## Transfer Of Data

We do not share or sell your data with third parties because we do not collect your data in the first place.

## Changes To This Privacy Policy

We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.

You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.

## Contact Us

If you have any questions about this Privacy Policy, please contact us:

* By email: daanvisa1@gmail.com
